export interface IWeather {
  temp: number;
  wind_speed: number;
}