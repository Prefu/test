import { ICity } from './city.model';

export interface ICities {
  cnt: number;
  list: ICity[];
}