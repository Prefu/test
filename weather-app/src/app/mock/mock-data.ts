import { ICities } from '../model/cities.model';
import { ICity } from '../model/city.model';

export const mockCity: ICity = {
  name: 'mock city',
  wind: {
    speed:1,
  },
  main: {
    temp: 1,
  },
  coord: {
    lat: 1,
    lon: 2,
  },
}

export const mockCities: ICities = {
  cnt : 1,
  list: [mockCity]
}