import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CitiesListComponent } from './components/cities-list/cities-list.component';
import { CityComponent } from './components/city/city.component';
import { HeaderComponent } from './components/header/header.component';
import { CityDetailsComponent } from './components/city-details/city-details.component';
import { MaterialModule } from './modules/material.module';
import { FooterComponent } from './components/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    CitiesListComponent,
    CityComponent,
    HeaderComponent,
    CityDetailsComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NoopAnimationsModule,
    MaterialModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
