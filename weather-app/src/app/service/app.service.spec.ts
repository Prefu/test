import { HttpErrorResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { mockCities } from 'src/app/mock/mock-data';
import { ICities } from '../model/cities.model';
import { IForecastcity } from '../model/forecast-city.model';
import { AppService } from './app.service';

describe('AppService', () => {
  let service: AppService;
  let httpMock: HttpTestingController;


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AppService]
    });

    service = TestBed.get(AppService);
    httpMock = TestBed.get(HttpTestingController);
  });


  it('should get data from getCity method',() => {
    service.getCity('bytom').subscribe((data: IForecastcity) => {
      expect(data.hourly[0].temp).toBe(1)
    });

    const req = httpMock.expectOne(`https://api.openweathermap.org/data/2.5//weather?q=bytom&appid=95e2ad7df26d242fea305d6800ff9ae7`);
    expect(req.request.method).toBe('GET');

    req.flush({
      hourly : [
        {
          temp: 1,
          wind_speed: 2,
        }
      ]
    });

    httpMock.verify();
  });

  it('should get data from getCities method',() => {
    service.getCities().subscribe((data: ICities) => {
      expect(data.list.length).toBe(1)
    });

    const req = httpMock.expectOne(`https://api.openweathermap.org/data/2.5//group?id=3101950,3096472,3181927,2968815,2643743&units=metric&appId=95e2ad7df26d242fea305d6800ff9ae7`);
    expect(req.request.method).toBe('GET');

    req.flush({
      data : mockCities
    });

    httpMock.verify();
  });
})
