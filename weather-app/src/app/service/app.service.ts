import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, filter, switchMap } from 'rxjs/operators';
import { ICities } from '../model/cities.model';
import { ICity } from '../model/city.model';
import { IForecastcity } from '../model/forecast-city.model';

@Injectable({ providedIn: 'root' })
export class AppService {
  constructor(private http: HttpClient) { }

  private apiUrl = 'https://api.openweathermap.org/data/2.5/';
  private apiKey = '95e2ad7df26d242fea305d6800ff9ae7';
  //Bytom, Katowice, 'Bologna', 'Paris, London
  private citiesIds = ['3101950','3096472','3181927','2968815', '2643743'].join(','); 

   getCity(city: string): Observable<IForecastcity> {
     return this.http.get<ICity>(`${this.apiUrl}/weather?q=${city}&appid=${this.apiKey}`).pipe(
       filter(city => !!city.coord),
       switchMap(data => {
          return this.http.get<IForecastcity>(`${this.apiUrl}/onecall?lat=${data.coord.lat}&lon=${data.coord.lon}&exclude=minutely,daily&units=metric&appid=${this.apiKey}`);
        }),
        catchError(error => of(error))
      )
   }

   getCities(): Observable<ICities> {
     return this.http.get<ICities>(`${this.apiUrl}/group?id=${this.citiesIds}&units=metric&appId=${this.apiKey}`).pipe(
       catchError(error => of(error))
     );
   }
}