import { Component, Input } from '@angular/core';
import { ICity } from 'src/app/model/city.model';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss']
})
export class CityComponent {
  @Input() city: ICity;
}
