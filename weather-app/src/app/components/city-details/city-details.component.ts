import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IForecastcity } from 'src/app/model/forecast-city.model';
import { AppService } from 'src/app/service/app.service';

@Component({
  selector: 'app-city-details',
  templateUrl: './city-details.component.html',
  styleUrls: ['./city-details.component.scss']
})
export class CityDetailsComponent implements OnInit {
  city$: Observable<IForecastcity>;

  constructor(
    private route: ActivatedRoute,
    private appService: AppService,
    public location: Location
  ) { }

  ngOnInit(): void {
    this.getCity();
  }

  getCity(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.city$ = this.appService.getCity(id);
  }

  goBack(): void {
    this.location.back();
  }
}
