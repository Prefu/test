import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { ICities } from 'src/app/model/cities.model';
import { AppService } from '../../service/app.service';

@Component({
  selector: 'app-cities-list',
  templateUrl: './cities-list.component.html',
  styleUrls: ['./cities-list.component.scss']
})

export class CitiesListComponent {
  cities$: Observable<ICities>;

  constructor(private appService: AppService) {
    this.cities$ = this.appService.getCities();
   }
}
