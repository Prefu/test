import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { mockCities } from 'src/app/mock/mock-data';
import { AppService } from 'src/app/service/app.service';
import { CitiesListComponent } from './cities-list.component';

describe('CitiesListComponent', () => {
  let component: CitiesListComponent;
  let fixture: ComponentFixture<CitiesListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CitiesListComponent ],
      imports: [HttpClientTestingModule],
      providers: [AppService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CitiesListComponent);
    component = fixture.componentInstance;
    component.cities$ = of(mockCities);
    fixture.detectChanges();
  });

  it('should create and initialize cities$ variable', () => {
    expect(component).toBeTruthy();
    expect(component.cities$).not.toBeUndefined();
  });
});
