# Overview

I created simple SPA where I present a list of 5 European cities.
When you click on the city you will be navigated to city details where you can see temperature in next 24 hours

## UI/UX

I used Angular Material library.
I know that this temperature should be shown in much better way. But to be honest I didn't work much with graphs libraries and didn't want to exceed limited 4 hours

## Unit tests

I created some basic unit tests.
Code coverage is above 90%:

Statements : 92.31% ( 36/39 )
Branches : 100% ( 0/0 )
Functions : 75% ( 9/12 )
Lines : 90.63% ( 29/32 )
